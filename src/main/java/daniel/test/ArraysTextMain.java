package daniel.test;
import java.util.Scanner;

public class ArraysTextMain {
    public static void main(String[] args){

        int [] array1 = {13, 34, 23, 23, 7, 12, 8, 3, 10, 5};

        for (int item:array1) {
            System.out.print(item + " ");
        }

        int sum=0;
        System.out.println();
        for (int item:array1) {
            sum = sum + item;
        }
        System.out.println("sum = " + sum);

        int product=1;
        for (int item:array1) {
            product = product * item;
        }
        System.out.println("product = " + product);

        int min = array1[0];
        for (int i = 1; i < array1.length ; i++) {
            if (array1[i] < min){
                min = array1[i];
            }
        }
        System.out.println("Min = " + min);

        int max = array1[0];
        for (int i = 1; i < array1.length ; i++) {
            if (array1[i] > max){
                max = array1[i];
            }
        }
        System.out.println("Max = " + max);

        int[] array2 = new int[5];

        for (int i = 0; i < array1.length ; i+=2) {
            int j = i / 2;
            array2[j]=array1[i];

        }
        for (int item:array2) {
            System.out.print(item + " ");
        }


    }
}
